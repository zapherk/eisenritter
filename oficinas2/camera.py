import cv2
import numpy
import RPi.GPIO as GPIO
import time

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

servoPIN = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

p = GPIO.PWM(servoPIN, 50) # GPIO 17 with 50Hz
p.start(2.5) # Initialization

#Pegar imagens da camera
cap = cv2.VideoCapture(0)

#Especificar kernel usado sobre a imagem
kernel = numpy.ones((5, 5), numpy.uint8)

#Para criar video do teste
#fourcc = cv2.VideoWriter_fourcc(*'XVID')
#out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))

try:
    while (True):

        # set Trigger to HIGH
        GPIO.output(GPIO_TRIGGER, True)

        # set Trigger after 0.01ms to LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER, False)

        #Pega frame por frame da camera
        ret, frame = cap.read()

        # save StartTime
        while GPIO.input(GPIO_ECHO) == 0:
            StartTime = time.time()

        # save time of arrival
        while GPIO.input(GPIO_ECHO) == 1:
            StopTime = time.time()

        # time difference between start and arrival
        TimeElapsed = StopTime - StartTime
        # multiply with the sonic speed (34300 cm/s)
        # and divide by 2, because there and back
        distance = (TimeElapsed * 34300) / 2

        #Criar mask para identificar alvo da cor (Blue, Green, Red)
        limitemax = numpy.array([30, 60, 255])
        limitemin = numpy.array([0, 15, 115])
        mask = cv2.inRange(frame, limitemin, limitemax)

        #Encontrar e desenhar o contorno
        opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
        x, y, w, h = cv2.boundingRect(opening)
        #cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 3)
        #cv2.circle(frame, (int (x+w/2), int (y+h/2)), 2, (0, 255, 0), -1)

        #Mostrar e registrar resultado
        #cv2.imshow('video', frame)
        #cv2.imshow('mask', mask)
        #out.write(frame)

        if w < 20:
            p.ChangeDutyCycle(10)
        else:
            p.ChangeDutyCycle(6)

        if distance < 3:
            break

        #Fechar e encerrar quando apertar esc(k == 27)
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

except KeyboardInterrupt:
    p.stop()
    GPIO.cleanup()
