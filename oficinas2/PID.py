import numpy
import RPi.GPIO as GPIO
import smbus
import time
import cv2

# Endereços dos registradores do MPU6050.
PWR_MGMT_1 = 0x6B
SMPLRT_DIV = 0x19
CONFIG = 0x1A
GYRO_CONFIG = 0x1B
INT_ENABLE = 0x38
ACCEL_XOUT_H = 0x3B
ACCEL_YOUT_H = 0x3D
ACCEL_ZOUT_H = 0x3F
GYRO_XOUT_H = 0x43
GYRO_YOUT_H = 0x45
GYRO_ZOUT_H = 0x47

# Atribuindo as direções do GPIO (IN / OUT).
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

# Configuração do servo motor.
servoPIN = 17
GPIO.setmode(GPIO.BCM)
GPIO.setup(servoPIN, GPIO.OUT)

StartTime = time.time()
StopTime = time.time()

p = GPIO.PWM(servoPIN, 50) # GPIO 17 com 50Hz.
p.start(2.5) # Inicialização.

# Pegar imagens da camera.
cap = cv2.VideoCapture(0)

# Especificar kernel usado sobre a imagem.
kernel = numpy.ones((5, 5), numpy.uint8)

# Para criar video do teste:
# fourcc = cv2.VideoWriter_fourcc(*'XVID')
# out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640, 480))

bus = smbus.SMBus(1)
Device_Address = 0x68  # Endereço do MPU6050.

def MPU_Init():
    # write to sample rate register.
    bus.write_byte_data(Device_Address, SMPLRT_DIV, 7)

    # Write to power management register.
    bus.write_byte_data(Device_Address, PWR_MGMT_1, 1)

    # Write to Configuration register.
    bus.write_byte_data(Device_Address, CONFIG, 0)

    # Write to Gyro configuration register.
    bus.write_byte_data(Device_Address, GYRO_CONFIG, 24)

    # Write to interrupt enable register.
    bus.write_byte_data(Device_Address, INT_ENABLE, 1)


def read_raw_data(addr):
    # Valores do acelerômetro e gyro são 16-bit.
    high = bus.read_byte_data(Device_Address, addr)
    low = bus.read_byte_data(Device_Address, addr + 1)

    # Concatena valores altos e baixos.
    value = ((high << 8) | low)

    # Para obter valores com sinal do MPU6050.
    if (value > 32768):
        value = value - 65536
    return value

# Configuração dos pinos do Raspberry Pi 3:

PWMPin1 = 37  # PWM Pino conectado ao ENA.
PWMPin2 = 40  # PWM Pino conectado ao ENB.
In1 = 35  # Conectado ao Input 1(motorA).
In2 = 33  # Conectado ao Input 2(motorA).
In3 = 36  # Conectado ao Input 3(motorB).
In4 = 38  # Conectado ao Input 4(motorB).

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

# Configurando o modo dos GPIOs para output:
GPIO.setup(PWMPin1, GPIO.OUT)
GPIO.setup(PWMPin2, GPIO.OUT)
GPIO.setup(In1, GPIO.OUT)
GPIO.setup(In2, GPIO.OUT)
GPIO.setup(In3, GPIO.OUT)
GPIO.setup(In4, GPIO.OUT)

GPIO.output(In1, GPIO.LOW)
GPIO.output(In2, GPIO.LOW)
GPIO.output(In3, GPIO.LOW)
GPIO.output(In4, GPIO.LOW)

# Configurando a frequência do PWM para 2000.
PwmValue1 = GPIO.PWM(PWMPin1, 2000)
PwmValue1.start(0)  # Esse é o valor máximo: 100 %.
PwmValue2 = GPIO.PWM(PWMPin2, 2000)
PwmValue2.start(0)

MPU_Init()


# Parâmetros do PID:
# Ganho Proporcional.
kp = 3
# Ganho Integral.
ki = 0
# Ganho Derivativo
kd = 3

# Inicializando os parâmetros.
antigo = time.time()
ultimaEntrada = 0
termoIntegral = 0

# Obtém a leitura inicial do acelerômetro.
acc_x = read_raw_data(ACCEL_XOUT_H)
acc_y = read_raw_data(ACCEL_YOUT_H)
acc_z = read_raw_data(ACCEL_ZOUT_H)

# Obtém a leitura inicial do giroscópio.
gyro_x = read_raw_data(GYRO_XOUT_H)
gyro_y = read_raw_data(GYRO_YOUT_H)
gyro_z = read_raw_data(GYRO_ZOUT_H)

# Full scale range +/- 250 degree/C as per sensitivity scale factor.
Ax = acc_x / 16384.0
Ay = acc_y / 16384.0
Az = acc_z / 16384.0
Gx = gyro_x / 131.0
Gy = gyro_y / 131.0
Gz = gyro_z / 131.0

# Pega um valor médio do giroscópio enquanto "parado" (velocidade angular de referência).
i = 0
while i < 100:
    gyro_x = gyro_x + read_raw_data(GYRO_XOUT_H)
    gyro_y = gyro_y + read_raw_data(GYRO_YOUT_H)
    gyro_z = gyro_z + read_raw_data(GYRO_ZOUT_H)
    acc_x = acc_x + read_raw_data(ACCEL_XOUT_H)
    acc_y = acc_y + read_raw_data(ACCEL_YOUT_H)
    acc_z = acc_z + read_raw_data(ACCEL_ZOUT_H)
    i = i + 1

gyro_x = gyro_x / 100
gyro_y = gyro_y / 100
gyro_z = gyro_z / 100

Gx_inicial = gyro_x / 131.0
Gy_inicial = gyro_y / 131.0
Gz_inicial = gyro_z / 131.0

acc_x = acc_x / 100
acc_y = acc_y / 100
acc_z = acc_z / 100

Ax_inicial = acc_x / 16384.0
Ay_inicial = acc_y / 16384.0
Az_inicial = acc_z / 16384.0

# Setpoint inicial (ângulo do robô em relação ao chão no instante inicial).
setpoint = numpy.arctan2(-Az_inicial, Ax_inicial) * 180 / numpy.pi
ultimaEntrada = setpoint
# Somente para visualização dos parâmetros no terminal.
print("Setpoint = %.2f" % setpoint)
print("Gx inicial = %.2f" % Gx_inicial)
print("Gy inicial = %.2f" % Gy_inicial)
print("Gz inicial = %.2f" % Gz_inicial)
print("Ax inicial = %.2f" % Ax_inicial)
print("Ay inicial = %.2f" % Ay_inicial)
print("Az inicial = %.2f" % Az_inicial)

# Começo do loop principal do programa.
try:
    while True:

        # Configura Trigger para HIGH.
        GPIO.output(GPIO_TRIGGER, True)

        # Configura Trigger após 0.01ms para LOW
        time.sleep(0.00001)
        GPIO.output(GPIO_TRIGGER, False)

        # Pega frame por frame da camera.
        ret, frame = cap.read()

        # Salva tempo de início.
        while GPIO.input(GPIO_ECHO) == 0:
            StartTime = time.time()

        # Salva tempo de chegada.
        while GPIO.input(GPIO_ECHO) == 1:
            StopTime = time.time()

        # Diferença de tempo entre início e chegada.
        TimeElapsed = StopTime - StartTime
        # Multiplica pela velocidade do som (34300 cm/s) e divide por 2 por ser o tempo de ir e voltar.
        distance = (TimeElapsed * 34300) / 2

        # Cria mask para identificar alvo da cor (Blue, Green, Red).
        limitemax = numpy.array([30, 60, 255])
        limitemin = numpy.array([0, 15, 115])
        mask = cv2.inRange(frame, limitemin, limitemax)

        # Encontra e desenha o contorno.
        opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
        x, y, w, h = cv2.boundingRect(opening)
        #cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 3)
        #cv2.circle(frame, (int (x+w/2), int (y+h/2)), 2, (0, 255, 0), -1)

        #Mostrar e registrar resultado
        #cv2.imshow('video', frame)
        #cv2.imshow('mask', mask)
        #out.write(frame)

        if w < 20:
            p.ChangeDutyCycle(10)
        else:
            p.ChangeDutyCycle(6)

        # Sai do loop quando encontra algo muito próximo do sensor.
        if distance < 3:
            break

        # Fecha e encerra se a tecla esc(k == 27) for pressionada no teclado.
        k = cv2.waitKey(1) & 0xFF
        if k == 27:
            break

        # Pega um valor do giroscopio e subtrai pelo valor médio inicial (valor prático).
        gyro_insta_x = read_raw_data(GYRO_XOUT_H)
        gyro_insta_y = read_raw_data(GYRO_YOUT_H)
        gyro_insta_z = read_raw_data(GYRO_ZOUT_H)

        gyro_insta_x = gyro_insta_x / 131.0
        gyro_insta_y = gyro_insta_y / 131.0
        gyro_insta_z = gyro_insta_z / 131.0

        Gx = gyro_insta_x - Gx_inicial
        Gy = gyro_insta_y - Gy_inicial
        Gz = gyro_insta_z - Gz_inicial

        print("Velocidade angular instantanea = %.2f" % Gx)

        # Pega um valor do acelerometro e extrai o ângulo instantâneo com arctan.
        acc_insta_x = read_raw_data(ACCEL_XOUT_H)
        acc_insta_y = read_raw_data(ACCEL_YOUT_H)
        acc_insta_z = read_raw_data(ACCEL_ZOUT_H)

        acc_insta_x = acc_insta_x / 16384.0
        acc_insta_y = acc_insta_y / 16384.0
        acc_insta_z = acc_insta_z / 16384.0

        Ax = acc_insta_x
        Ay = acc_insta_y
        Az = acc_insta_z

        angulo = numpy.arctan2(-Az, Ax) * 180 / numpy.pi
        print("Az = %.2f" % Az)
        print("Ax = %.2f" % Ax)
        print("Angulo instantaneo = %.2f" % angulo)

        # Pega o valor prático do giroscópio e multiplica este pela diferença de tempo desta amostragem
        # em relação a última (variação prática do angulo).
        atual = time.time()
        delta = atual - antigo
        antigo = atual
        delta_angulo = Gx * delta
        print("Delta = %.2f" % delta)

        # Filtro.
        entrada = (angulo + (delta_angulo + setpoint)) / 2
        print("Entrada = %.2f" % entrada)

        # PID.
        erro = setpoint - entrada
        termoIntegral = termoIntegral + erro
        termoDerivada = entrada - ultimaEntrada
        saida = kp * erro + ki * termoIntegral + kd * termoDerivada
        ultimaEntrada = entrada
        print("kp = %.f" % erro)
        print("ki = %.f" % termoIntegral)
        print("kd = %.f" % termoDerivada)
        print("Saida = %.f" % saida)

        # PWM.
        if saida > 0:
            GPIO.output(In1, GPIO.HIGH)
            GPIO.output(In2, GPIO.LOW)
            GPIO.output(In4, GPIO.HIGH)
            GPIO.output(In3, GPIO.LOW)
        else:
            GPIO.output(In1, GPIO.LOW)
            GPIO.output(In2, GPIO.HIGH)
            GPIO.output(In4, GPIO.LOW)
            GPIO.output(In3, GPIO.HIGH)

        if abs(saida) > 100:
            PwmValue1.ChangeDutyCycle(100)
            PwmValue2.ChangeDutyCycle(100)
        else:
            PwmValue1.ChangeDutyCycle(abs(saida))
            PwmValue2.ChangeDutyCycle(abs(saida))

        # Deu ruim.
        if abs(erro) > 60:
            break


except KeyboardInterrupt:
    PwmValue1.stop()
    PwmValue2.stop()
    GPIO.cleanup()

PwmValue1.stop()
PwmValue2.stop()
p.stop()
GPIO.cleanup()
